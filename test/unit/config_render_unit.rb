require 'minitest/autorun'
require_relative '../../libraries/carbon_config'

class TestCarbonConfig < Minitest::Test
  TEST_CONFIG = {
    first_section: {
      first: 'string',
      second: 1234,
      third: '"/regexp/"'
    },
    second_section: {
      'CAPITALIZED' => 'string',
      retentions: '15s:7d,1m:21d,15m:5y',
      pattern: '^servers\.www.*\.workers\.busyWorkers$'
    }
  }

  INI_RESULT =
    '
[first_section]
first = string
second = 1234
third = "/regexp/"

[second_section]
CAPITALIZED = string
retentions = 15s:7d,1m:21d,15m:5y
pattern = ^servers\.www.*\.workers\.busyWorkers$
'

  def setup
    @config = TEST_CONFIG
    @rendered_text = ChefGraphite::Config.hash_to_ini(@config)
  end

  def test_render
    assert_equal(INI_RESULT, @rendered_text)
  end
end
