# CHANGELOG for graphite-test

This file is used to list changes made in each version of graphite-test.

## 0.1.0:

* Initial release of graphite-test

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
