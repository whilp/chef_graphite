name 'graphite-test'
maintainer 'Mathieu Sauve-Frankel'
maintainer_email 'msf@qti.qualcomm.com'
license 'All rights reserved'
description 'Installs/Configures graphite-test'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '0.1.0'
depends 'apt'
