
module ChefGraphite
  # Helps generate ini format configuration files for Carbon
  module Config
    # Takes a Enumerable and spits out a ini file.
    # Hash should contain hashes named for their ini section.
    # Section hashes contain items to be configured and their values.
    #
    #  ```
    #  foo = {
    #    garbage_collection: {
    #      pattern: 'garbageCollections$',
    #      retentions: '10s:14d'
    #    }
    #  }
    #  ```
    #
    # Produces
    #
    # ```
    # [garbage_collection]
    # pattern = garbageCollections$
    # retentions = 10s:14d
    # ```
    #
    # @param config type [Enumerable]
    # @return 'mold' the formatted string.
    def self.hash_to_ini(config)
      mold = ''
      config.each do |section, settings|
        mold << "\n[#{ section }]\n"
        settings.each do |key, value|
          mold << "#{ key } = #{ value }\n"
        end
      end
      mold
    end
  end
end
