
file "#{node[:graphite][:conf_dir]}/carbon.conf" do
  owner node[:graphite][:user]
  group node[:graphite][:group]
  mode 00644
  content lazy {
    ChefGraphite::Config.hash_to_ini(node[:graphite][:carbon][:config])
  }
end
