include_recipe 'runit'

runit_service 'carbon-cache' do
  options({
    :lock_dir => node[:graphite][:carbon][:lock_dir]
  })
end
