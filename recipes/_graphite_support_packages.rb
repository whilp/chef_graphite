include_recipe 'python'
include_recipe 'build-essential'

node[:graphite][:packages].map do |pkg, ver|
  python_pip pkg do
    version ver
    virtualenv node[:graphite][:virtualenv]
    action :install
  end
end
